cmake_minimum_required(VERSION 3.0)

find_package(PkgConfig REQUIRED)
pkg_check_modules(CppUTest QUIET cpputest)

# Add the CppUTest::CppUTest new library.
if(CppUTest_FOUND AND NOT TARGET CppUTest::CppUTest)
    # Find the original CppUTest library.
    find_library(
        CPPUTEST_LIBRARY NAMES CppUTest
        PATHS ${PC_CppUTest_LIBDIR} ${PC_CppUTest_LIBRARY_DIRS})
    add_library(CppUTest::CppUTest INTERFACE IMPORTED)
    # Set the library's include dirs and linked libraries.
    set_target_properties(CppUTest::CppUTest PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${CppUTest_INCLUDE_DIRS}")
    set_target_properties(CppUTest::CppUTest PROPERTIES
        INTERFACE_LINK_LIBRARIES "${CPPUTEST_LIBRARY}") 
endif()

# Add the CppUTest::CppUTestExt new library.
if(CppUTest_FOUND AND NOT TARGET CppUTest::CppUTestExt)
    # Find the original CppUTestExt library.
    find_library(
        CPPUTESTEXT_LIBRARY NAMES CppUTestExt
        PATHS ${PC_CppUTest_LIBDIR} ${PC_CppUTest_LIBRARY_DIRS})
    add_library(CppUTest::CppUTestExt INTERFACE IMPORTED)
    # Set the library's include dirs and linked libraries.
    set_target_properties(CppUTest::CppUTestExt PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${CppUTest_INCLUDE_DIRS}")
    set_target_properties(CppUTest::CppUTestExt PROPERTIES
        INTERFACE_LINK_LIBRARIES "${CPPUTESTEXT_LIBRARY}") 
endif()
